//personne model

function Personne(prenom, nom, pseudo) {
  this.prenom = prenom;
  this.nom = nom;
  this.pseudo = pseudo;
  this.getNomComplet = function() {
    return "nom : "+this.nom +" prenom : "+ this.prenom + " pseudo : "+this.pseudo;
  };
};

function Client(prenom, nom, pseudo, numeroClient) {

Personne.call(this,prenom, nom, pseudo)

  this.numeroClient = numeroClient;

  this.getNomComplet = this.getNomComplet = function() {
    return "nom : "+this.nom +" prenom : "+ this.prenom + " pseudo : "+this.pseudo;
  };
  this.getInfo = function() {
    return "numero Client : "+this.numeroClient + " nom : "+this.nom +" prenom : "+ this.prenom;
  };


};




// instence
var jules = new Personne('Jules', 'LEMAIRE','jules77');
var paul = new Personne("Paul","LEMAIRE","paul44");


ident(jules);
ident(paul);

jules.pseudo="jules44";
ident(jules);

console.log("age de jule :"+jules.age);
Personne.prototype.age="NON DEFINI";
console.log("age de jule :"+jules.age);
jules.age=20;
console.log("age de jule :"+jules.age);

Personne.prototype.getInitiale=function(){
  return this.prenom.charAt(0).toUpperCase()+" "+this.nom.charAt(0).toUpperCase();
};
console.log("initiale de jule :"+jules.getInitiale());




console.log("############ sans constructeur ############");

var robert={
  prenom: "Robert",
  nom : "LEPREFET",
  pseudo : "robert77",
  getNomComplet : function(){
    return "nom : "+this.nom +" prenom : "+ this.prenom + " pseudo : "+this.pseudo;
  }

}

ident(robert);


// instence
var steve = new Client('steve', 'LUCAS','steve44', "A01");
ident(steve);

console.log(steve.numeroClient);
console.log(steve.getInfo());






//function reutilisable
function ident(objet){
  console.log(objet.nom);
  console.log(objet.prenom);
  console.log(objet.pseudo);
  console.log(objet.getNomComplet());
  console.log("##########################");
}
