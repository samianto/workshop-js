const table = document.querySelector('table.utilisateur');
const req = new Request('https://loisirs-web-backend.cleverapps.io/users');

fetch(req)
  .then(response => response.json())
  .then(data => {
    for (const user of data.users) {
      let tableItem = document.createElement('tr');
      tableItem.appendChild(document.createElement('td')).textContent = user.id;
      tableItem.appendChild(document.createElement('td')).textContent = user.nom;
      tableItem.appendChild(document.createElement('td')).textContent = user.pass;
      table.appendChild(tableItem);
    }
  });
