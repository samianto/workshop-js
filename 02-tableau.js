var villes=['nantes', 'paris', 'saint-nazaire', 'angers', 'le mans'];
var containA = (data) => data.includes("a");
var containTiret = (data) => data.includes("-");
var sansTiret = (data) => !data.includes("-");
var sansEspace = (data) => !data.includes(" ");
var derniereLettreS=(data => data[data.length-1]=="s")


villes.forEach((ville, i) => {
console.log("ville "+(i+1)+" : "+ville);
});

console.log("Lettre A dans toutes les villes : "+villes.every(containA));
console.log("un tiret dans au moins une ville : "+villes.some(containTiret));

var villesSansTiretSansEspace =villes.filter(sansTiret).filter(sansEspace);
console.log("villes Sans Tiret Sans Espace : "+villesSansTiretSansEspace);

var newTableau=villes.filter(derniereLettreS);
newTableau.forEach((ville, i) => {
newTableau[i]=ville.toUpperCase();
});

console.table(newTableau);
